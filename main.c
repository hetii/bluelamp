/*--------------------------------------------------------------------------*
* Bluetooth UART interface implementation for Attiny13
*---------------------------------------------------------------------------*
* 2015-01-06
*
* Half-duplex 8N1 serial UART in software.
*
* This code based on following links:
* https://github.com/thegaragelab/tinytemplate/tree/master/firmware
* http://we.easyelectronics.ru/AVR/uart-programmnyy-na-atiny13a.html
* http://thegaragelab.com/using-the-hc-05-bluetooth-breakout/
*
*--------------------------------------------------------------------------*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define BAUD_RATE 9600
#define UART_TX PINB4
#define UART_RX PINB3

/* account for integer truncation by adding 3/2 = 1.5 */
# define TXDELAY (int)(((F_CPU/BAUD_RATE)-7 +1.5)/3)
# define RXDELAY (int)(((F_CPU/BAUD_RATE)-5 +1.5)/3)
# define RXDELAY2 (int)((RXDELAY*1.5)-2.5)
# define RXROUNDED (((F_CPU/BAUD_RATE)-5 +2)/3)
# if RXROUNDED > 127
# error low baud rates unsupported - use higher BAUD_RATE
# endif

// Initialise the UART
void uart_init() {
    // Set as input and disable pullup
    DDRB  &= ~(1 << UART_RX);
    PORTB &= ~(1 << UART_RX);
    // Set up TX pin
    DDRB |= (1 << UART_TX);
    PORTB |= (1 << UART_TX);
}

char uart_recv() {
    char ch;
    // Set as input and disable pullup
    DDRB  &= ~(1 << UART_RX);
    PORTB &= ~(1 << UART_RX);
    // Read the byte
    cli();
    asm volatile(
        "  ldi r18, %[rxdelay2]              \n\t" // 1.5 bit delay
        "  ldi %0, 0x80                      \n\t" // bit shift counter
        "WaitStart:                          \n\t"
        "  sbic %[uart_port]-2, %[uart_pin]  \n\t" // wait for start edge
        "  rjmp WaitStart                    \n\t"
        "RxBit:                              \n\t"
        // 6 cycle loop + delay - total = 5 + 3*r22
        // delay (3 cycle * r18) -1 and clear carry with subi
        "  subi r18, 1                       \n\t"
        "  brne RxBit                        \n\t"
        "  ldi r18, %[rxdelay]               \n\t"
        "  sbic %[uart_port]-2, %[uart_pin]  \n\t" // check UART PIN
        "  sec                               \n\t"
        "  ror %0                            \n\t"
        "  brcc RxBit                        \n\t"
        "StopBit:                            \n\t"
        "  dec r18                           \n\t"
        "  brne StopBit                      \n\t"
        : "=r" (ch)
        : [uart_port] "I" (_SFR_IO_ADDR(PORTB)),
          [uart_pin] "I" (UART_RX),
          [rxdelay] "I" (RXDELAY),
          [rxdelay2] "I" (RXDELAY2)
        : "r0","r18","r19");
    sei();
    return ch;
}

/** Write a single character
 * Send a single character on the UART.
 * @param ch the character to send.
 */
void uart_send(char ch) {
    // Set to output state and bring high
    PORTB |= (1 << UART_TX);
    cli();
    asm volatile(
        "  cbi %[uart_port], %[uart_pin]    \n\t"  // start bit
        "  in r0, %[uart_port]              \n\t"
        "  ldi r30, 3                       \n\t"  // stop bit + idle state
        "  ldi r28, %[txdelay]              \n\t"
        "TxLoop:                            \n\t"
        // 8 cycle loop + delay - total = 7 + 3*r22
        "  mov r29, r28                     \n\t"
        "TxDelay:                           \n\t"
        // delay (3 cycle * delayCount) - 1
        "  dec r29                          \n\t"
        "  brne TxDelay                     \n\t"
        "  bst %[ch], 0                     \n\t"
        "  bld r0, %[uart_pin]              \n\t"
        "  lsr r30                          \n\t"
        "  ror %[ch]                        \n\t"
        "  out %[uart_port], r0             \n\t"
        "  brne TxLoop                      \n\t"
        :
        : [uart_port] "I" (_SFR_IO_ADDR(PORTB)),
          [uart_pin] "I" (UART_TX),
          [txdelay] "I" (TXDELAY),
          [ch] "r" (ch)
        : "r0","r28","r29","r30");
    sei();
}

int main(void){
    //PB0, PB1, PB2 as aoutput.
    DDRB  = (1<<0)|(1<<1)|(1<<2);
    PORTB = 0;
    uart_init();
    char x;
    uint8_t i=0, k=0, loop=0;
    char buf[50];

    while (1){
        x = uart_recv();
        if ((x == '\n')||(x == '\r')) continue;
        buf[i++] = x;
        if (i > 50){
            i = 0;
        }

        switch(x){
            case 0x01:
                PORTB ^= (1<<0);
                break;
            case 0x02:
                PORTB ^= (1<<1);
                break;
            case 0x03:
                PORTB ^= (1<<2);
                break;
            case 0x04:
                uart_send((PINB & ((1<<0)|(1<<1)|(1<<2))));
                break;
            case 0x05:
                loop = 0;
                for (k=0; k<i-1; k++){
                    uart_send(buf[k]);
                }
                i = 0;
                break;
            case 0x06:
                loop = 0;
                _delay_ms(9000);
                /*uint8_t ask = 1;
                while(ask){
                    _delay_ms(1000);
                    uart_send('A');
                    uart_send('T');
                    if ((uart_recv() == 'O') && (uart_recv() == 'K')) ask = 0;
                }*/
                for (k=0; k<i-1; k++){
                    uart_send(buf[k]);
                }
                i = 0;
                break;
            case 0x07:
                loop = 1;
                break;
            case 0x08:
                loop = 0;
                break;
            default:
                if (loop == 1) uart_send(x);
                break;
        }
        //_delay_ms(10);
    }
}
